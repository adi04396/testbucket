import { SET_CURRENT_USER, SET_CURRENT_LOC } from "../actions/types";

  const isEmpty = require("is-empty");

  const initialState = {
    isAuthenticated: false,
    user: {},
    location: {},
  };

  export default function(state = initialState, action) {
    switch (action.type) {
      case SET_CURRENT_USER:
        return {
          ...state,
          isAuthenticated: !isEmpty(action.payload),
          user: action.payload
        };
      case SET_CURRENT_LOC:
        return {
          ...state,
          location: action.payload
        };
      default:
        return state;
    }
  }
