import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import classnames from "classnames";

class Register extends Component{
    constructor(){
        super();
        this.state = {
            name:"",
            email:"",
            password:"",
            password2:"",
            phone:"",
            zipcode:"",
            mobile:"",
            profilepic: "",

            errors:{}
        }
    }
    componentDidMount() {
        if (this.props.auth.isAuthenticated) {
          this.props.history.push("/dashboard");
        }
      }

    componentWillReceiveProps(nextProps) {
        if (nextProps.errors) {
          this.setState({
            errors: nextProps.errors
          });
        }
      }

    onChange = e => {
        this.setState({[e.target.id]:e.target.value})
    }

    filechange = event => {
      this.setState({ profilepic: event.target.files[0] });
   };

    onSubmit = e => {
        e.preventDefault();

        const formData = new FormData();
        formData.append("profilepic", this.state.profilepic);
        formData.append('name', this.state.name);
        formData.append('email', this.state.email);
        formData.append('password', this.state.password);
        formData.append('password2', this.state.password2);
        formData.append('phone', this.state.phone);
        formData.append('zipcode', this.state.zipcode);
        formData.append('mobile', this.state.mobile);

        this.props.registerUser(formData, this.props.history);
    }

    render(){
        const {name, email, password, password2, phone, mobile, zipcode, profilepic ,errors} = this.state;

        return(
            <div className="form-box ">
            <form className="signup-form"  onSubmit={this.onSubmit}>

               <div><Link to="/"><i className="fa fa-arrow-circle-left  "></i> Back to Home</Link></div>

                <h2>Register</h2>
                <hr/>
                <div className="form-group">
                    <input type="text"
                           id="name"
                           placeholder="Name"
                           value={name}
                           error={errors.name}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.name
                          })}/>
                        <span className="red-text">{errors.name}</span>

                </div>

                <div className="form-group">
                    <input type="email"
                           id="email"
                           placeholder="Email Address"
                           value={email}
                           error={errors.email}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.email
                          })}/>
                    <span className="red-text">{errors.email}</span>
                </div>

                <div className="form-group">
                    <input type="password"
                           id="password"
                           placeholder="Password"
                           value={password}
                           error= {errors.password}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.password
                          })} />
                    <span className="red-text">{errors.password}</span>
                </div>

                <div className="form-group">
                    <input type="password"
                           id="password2"
                           placeholder="Confirm Password"
                           value={password2}
                           error={errors.password}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.password2
                          })} />
                    <span className="red-text">{errors.password2}</span>
                </div>

                <div className="form-group">
                    <input type="text"
                           id="phone"
                           placeholder="phone"
                           value={phone}
                           onChange={this.onChange}
                           className={classnames("form-control")}
                           />
                </div>

                <div className="form-group">
                    <input type="text"
                           id="mobile"
                           placeholder="Mobile"
                           value={mobile}
                           error={errors.mobile}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.mobile
                          })} />
                    <span className="red-text">{errors.mobile}</span>
                </div>

                <div className="form-group">
                    <input type="text"
                           id="zipcode"
                           placeholder="ZipCode"
                           value={zipcode}
                           error={errors.zipcode}
                           onChange={this.onChange}
                           className={classnames("form-control", {
                            invalid: errors.zipcode
                          })}/>
                        <span className="red-text">{errors.zipcode}</span>

                </div>

                <div className="form-group">
                    <input type="file"
                           id="profile"
                           name="profilepic"
                           onChange={this.filechange}
                           />

                </div>

                <div className="form-group">
                    <button type="submit" className="btn btn-primary btn-block btn-lg">Sign Up</button>
                </div>
                <div className="text-center">Already have an account? <Link to="/login">Login here</Link></div>

            </form>
        </div>
        )
    }
}

Register.propTypes = {
    registerUser: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    errors: PropTypes.object.isRequired
  };

const mapStateToProps = state => ({
    auth: state.auth,
    errors: state.errors
  });

export default connect(mapStateToProps,{registerUser})(withRouter(Register));
