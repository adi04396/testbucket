import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser, fetchUserlatlong } from "../../actions/authActions";

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      shownearestFive: false
    }
  }

  onLogout = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  showprofile = e =>  {
    e.preventDefault();
    this.props.history.push("/profile");
  };

  showNearestFive = e => {
    e.preventDefault();
    var formData = new FormData();
    formData.append('email', this.props.auth.user.email);
    console.log(this.props.auth.user.email);
    this.props.fetchUserlatlong(formData);
  };

render() {
    const { user, location } = this.props.auth;
    console.log('loc', location);
    const { shownearestFive } = this.state
    return (
      <div  className="container text-center mt-15">
        <div className="row">
          <div className="col-sm-12">
            <h4>
              Hey there, <b className="name-lable">{user.name.split(" ")[0]}</b>
              <p className="mt-4">
                You are logged into a full-stack{" "}
                <span style={{ fontFamily: "monospace" }}>MERN</span> app 👏
              </p>
            </h4>
            <button
              onClick={this.showNearestFive}
              className="btn btn-large btn-light hoverable font-weight-bold"
            >
            Show Users to nearest location
            </button>
            <button
              onClick={this.showprofile}
              className="btn btn-large btn-light hoverable font-weight-bold"
            >
            Show Profile
            </button>
            <button
              onClick={this.onLogout}
              className="btn btn-large btn-light hoverable font-weight-bold"
            >
              Logout
            </button>
          </div>
          <div>
          { location.length > 0 ?
            location.map((item, i) => {
              return [
                  
                  <tr key={i}>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>{item.phone}</td>
                  </tr>
                ];
            })
            : 'No data found'
           }
          </div>
        </div>
      </div>
    );
  }
}

Dashboard.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  fetchUserlatlong: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser, fetchUserlatlong }
)(Dashboard);
