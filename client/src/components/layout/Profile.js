import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUser } from "../../actions/authActions";
import classnames from "classnames";

class Profile extends Component {

  constructor(){
      super();
      this.state = {
          name:"",
          email:"",
          phone:"",
          zipcode:"",
          mobile:"",
          profilepic: "",

          errors:{}
      }
  }

   componentWillReceiveProps(nextProps) {
      if (nextProps.errors) {
        this.setState({
          errors: nextProps.errors
        });
      }
    }


   componentDidMount() {
       console.log('eewre', this.props.auth);
       this.setState({
         name:this.props.auth.user.name,
         email:this.props.auth.user.email,
         phone:this.props.auth.user.phone,
         zipcode:this.props.auth.user.zipcode,
         mobile:this.props.auth.user.mobile,
         profilepic: "",
       })
    }

  onChange = e => {
      this.setState({[e.target.id]:e.target.value})
  }


  filechange = event => {
    this.setState({ profilepic: event.target.files[0] });
  };

  onSubmit = e => {
      e.preventDefault();
      console.log(this.state)

      const formData = new FormData();

      formData.append("profilepic", this.state.profilepic);
      formData.append('name', this.state.name);
      formData.append('email', this.state.email);
      formData.append('phone', this.state.phone);
      formData.append('zipcode', this.state.zipcode);
      formData.append('mobile', this.state.mobile);
      console.log('formData', formData);

      this.props.updateUser(formData);
  }


render() {
  const {name, email, phone, mobile, zipcode, profilepic ,errors} = this.state;

  return(
        <div className="form-box ">
        <form className="signup-form"  onSubmit={this.onSubmit}>

           <div><Link to="/dashboard"><i className="fa fa-arrow-circle-left  "></i> Back to Home</Link></div>

            <h2>Update Profile</h2>
            <hr/>
            <div className="form-group">
                <input type="text"
                       id="name"
                       placeholder="Name"
                       value={name}
                       error={errors.name}
                       onChange={this.onChange}
                       className={classnames("form-control", {
                        invalid: errors.name
                      })}/>
                    <span className="red-text">{errors.name}</span>

            </div>

            <div className="form-group">
                <input type="email"
                       id="email"
                       placeholder="Email Address"
                       value={email}
                       error={errors.email}
                       onChange={this.onChange}
                       className={classnames("form-control", {
                        invalid: errors.email
                      })}/>
                <span className="red-text">{errors.email}</span>
            </div>

            <div className="form-group">
                <input type="text"
                       id="phone"
                       placeholder="phone"
                       value={phone}
                       onChange={this.onChange}
                       className={classnames("form-control")}
                       />
            </div>

            <div className="form-group">
                <input type="text"
                       id="mobile"
                       placeholder="Mobile"
                       value={mobile}
                       error={errors.mobile}
                       onChange={this.onChange}
                       className={classnames("form-control", {
                        invalid: errors.mobile
                      })} />
                <span className="red-text">{errors.mobile}</span>
            </div>

            <div className="form-group">
                <input type="text"
                       id="zipcode"
                       placeholder="ZipCode"
                       value={zipcode}
                       error={errors.zipcode}
                       onChange={this.onChange}
                       className={classnames("form-control", {
                        invalid: errors.zipcode
                      })}/>
                    <span className="red-text">{errors.zipcode}</span>

            </div>

            <div className="form-group">
                <input type="file"
                       id="profile"
                       name="profilepic"
                       onChange={this.filechange}
                       />

            </div>

            <div className="form-group">
                <button type="submit" className="btn btn-primary btn-block btn-lg">Update</button>
            </div>
            <div className="text-center"><Link to="/dashboard">Dashboard</Link></div>

        </form>
    </div>
    )
  }
}

Profile.propTypes = {
  updateUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
};


function mapStateToProps(state) {
  console.log('state', state);
  const { auth } = state
  console.log('state', auth);
  // const {get_project:get_project  } =  ProjectaddReducer
	// console.log('data', get_project)
  return {auth}
}

export default connect( mapStateToProps, {updateUser} )(Profile);
