import axios from "axios";
import setAuthToken from "../util/setAuthToken";
import jwt_decode from "jwt-decode";

import { GET_ERRORS, SET_CURRENT_USER, GET_PROFILE, SET_CURRENT_LOC } from "./types";

//Register User
export const registerUser = (userData, history) => dispatch => {
  console.log('userData', userData);
  const config = {
      headers: { 'content-type': 'multipart/form-data' }
  }
  axios.post("http://localhost:5000/api/users/register", userData, config)
  .then(res => history.push("/login"))
  .catch(err=> dispatch({
    type:GET_ERRORS,
    payload:err.response.data
  }))
}

//Register User
export const updateUser = (userData, history) => dispatch => {
  console.log('userData', userData);
  const config = {
      headers: { 'content-type': 'multipart/form-data' }
  }
  axios.post("http://localhost:5000/api/users/updateUser", userData, config)
  .then(res => {
    console.log(res.data.json);
    dispatch(setCurrentUser(res.data.json));
  })
  .catch(err=> dispatch({
    type:GET_ERRORS,
    payload:err.response.data
  }))
}

//Fetch user
export const fetchUser = (email) => dispatch => {
  console.log('email in action', email)
  axios.get("http://localhost:5000/api/users/fetchUserDetails",email)
  .then( res => {
      console.log('here i am', res);
   }
  )
  .catch(err=> dispatch({
    type:GET_ERRORS,
    payload:err.response.data
  }))
}


//Login
export const loginUser = (userData) => dispatch => {
  const config = {
      headers: { 'content-type': 'multipart/form-data' }
  }
  axios.post("http://localhost:5000/api/users/login",userData, config)
  .then( res => {
      console.log(res);
      const {token} = res.data;
      // Set token to localStorage
      localStorage.setItem("jwtToken", token);
      // Set token to Auth header
      setAuthToken(token);
      const decoded = jwt_decode(token);
      console.log('decoded', decoded);
      // Set current user
      dispatch(setCurrentUser(decoded));
   }
  )
  .catch(err=> dispatch({
    type:GET_ERRORS,
    payload:err.response.data
  }))
}

//Register User
export const fetchUserlatlong = (userData, history) => dispatch => {
  console.log('userData', userData);
  const config = {
      headers: { 'content-type': 'multipart/form-data' }
  }
  axios.post("http://localhost:5000/api/users/fetchdatabasedonlatlng", userData, config)
  .then(res => {
    console.log(res.data.json);
    // // Set current user
    dispatch(setCurrentlocvalue(res.data.json));
  })
  .catch(err=> dispatch({
    type:GET_ERRORS,
    payload:err.response.data
  }))
}


// Set logged in user
export const setCurrentUser = decoded_data => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded_data
  };
};

export const setCurrentlocvalue = decoded_data => {
  return {
    type: SET_CURRENT_LOC,
    payload: decoded_data
  };
};

// Logout user
export const logoutUser = () => dispatch => {
  // Remove token from local storage
  localStorage.removeItem("jwtToken");
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to empty object {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};
