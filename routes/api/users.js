const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var bodyParser = require('body-parser');
const keys = require("../../config/keys");
const multer = require('multer');
const path = require('path');
const helpers = require('./helpers');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __dirname + 'uploads')
    },

    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

const fileFilter = (req,file,cb) => {
    console.log('file', file);
    if(file.mimetype === "image/jpg"  ||
       file.mimetype ==="image/jpeg"  ||
       file.mimetype ===  "image/png"){

    cb(null, true);
   } else {
      cb(new Error('Only image files are allowed!'), false);
    }
}

const uploaddata = multer({storage: storage, fileFilter : fileFilter});

// Load input validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");


// Load User model
const User = require("../../models/UserSchema");

// @route POST api/users/register
// @desc Register user
// @access Public

router.post("/register", uploaddata.single('profilepic'), async(req,
            res, next) => {
      //Form validation
      const {errors, isValid} = validateRegisterInput(req.fields);

      if(!isValid){
          return res.status(400).json(errors);
      }

      User.findOne({email:req.fields.email}).then(user=>{

          if(user) {
              return res.status(400).json({email:"Email already exists"});
          } else {
            // console.log(req.fields);
            const newUser = new User({
                name:req.fields.name,
                password:req.fields.password,
                email:req.fields.email,
                phone: req.fields.phone,
                zipcode: req.fields.zipcode,
                mobile: req.fields.mobile,
                loc:{
                  type:"Point",
                  coordinates:[-110.8571443, 32.4586858]
                }
            });

            // Hash password before storing in database
            const rounds  = 10;
            bcrypt.genSalt(rounds, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) throw err;
                newUser.password = hash;
                newUser
                    .save()
                    .then(user => res.json(user))
                    .catch(err => console.log(err));
                });
            });


           }

      });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public

router.post("/login", async(req,res , next) => {

    //Form Valdiation
    console.log('REQUEST CAME');
    console.log(req.fields)
    // const {errors, isValid} = validateLoginInput(req.body);
    //
    // if (!isValid) {
    //     return res.status(400).json(errors);
    // }

    const email = req.fields.email;
    console.log('email', email);
    const password = req.fields.password;

    //Find user by Email
    User.findOne({email}).then(user=>{
        if(!user){
            return res.status(404).json({ emailnotfound: "Email not found" });
        }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
        if (isMatch) {

            console.log(user);
            // Create JWT Payload
            const payload = {
                id: user.id,
                name: user.name,
                email: user.email,
                phone: user.phone,
                zipcode: user.zipcode,
                mobile: user.mobile
            };

            // Sign token
            jwt.sign(
                payload,
                keys.secretOrKey,
                {
                 expiresIn: 31556926
                },
                (err, token) => {
                res.json({
                    success: true,
                    token: "Bearer " + token
                });
                }
            );
        } else {
          return res
            .status(400)
            .json({ passwordincorrect: "Password incorrect" });
        }
      });
    });
});

// @route POST api/users/login
// @desc Login user and return JWT token
// @access Public

router.post("/updateUser", async(req,res , next) => {
    console.log('I CAME HERE', req.fields);
    const email = req.fields.email;
    console.log('email', email);
    //Find user by Email
    User.findOne({email}).then(user=> {
        if(!user){
            return res.status(404).json({ emailnotfound: "Email not found" });
        }

        var myquery = { email: email };
        var newvalues = { $set: {name:req.fields.name, password:user.password, email:req.fields.email, phone: req.fields.phone, zipcode: req.fields.zipcode, mobile: req.fields.mobile } };

        // var newvalues = { $set: {loc:{type:"Point",
        //         coordinates:[parseFloat(req.fields.lat), parseFloat(req.fields.lang)]}} };

        const payload = {
            id: user.id,
            name: req.fields.name,
            email: user.email,
            phone: req.fields.phone,
            zipcode: req.fields.zipcode,
            mobile: req.fields.mobile
        };

        User.updateOne(myquery, newvalues).then(user => res.json({user, json: payload})).catch(err => console.log(err));
      });
});

// @route POST api/users/fetchUser
// @desc Login user and return JWT token
// @access Public
router.post("/fetchdatabasedonlatlng", async(req,res , next) => {
    console.log('fetch data based on lat long');
    console.log(req.fields);
    const email = req.fields.email;
    //Find user by Email
    var milesToRadian = function(miles){
        var earthRadiusInMiles = 3959;
        return miles / earthRadiusInMiles;
    };
    User.findOne({email: email}).then(landmark => {
        console.log(landmark);
        var query = {
            "loc" : {
                $geoWithin : {
                    $centerSphere : [landmark.loc.coordinates, milesToRadian(500) ]
                }
            }
        };
        // Step 3: Query points.
        User.find(query).limit( 5 ).then(user => {
          res.json({
              success: true,
              json: user
          });
        });
    });

});


module.exports = router;
