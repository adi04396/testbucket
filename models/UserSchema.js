const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  zipcode: {
    type: String,
    required: true
  },
  mobile: {
    type: String,
    required: true
  },
  loc: {
      type: { type: String },
      coordinates: []
  },
  date: {
    type: Date,
    default: Date.now
  }
});

UserSchema.index({ "loc": "2dsphere" });

module.exports = mongoose.model("users", UserSchema);
